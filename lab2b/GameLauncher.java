import java.util.Scanner;

public class GameLauncher{
	
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		boolean notchosen = true;
		System.out.println("Hello, type one (1) to play hangman, or two (2) to play wordle.");
		while (notchosen){
		int option = scan.nextInt();
		if(option==1){
			hangmanMain();
			notchosen = false;
		}else if(option==2){
			wordleMain();
			notchosen = false;
		}else{
			System.out.println("You didn't type either 1 or two, please type one (1) to play hangman or two (2) to play wordle.");
		}
	}
	}
	
	//asks for word and starts the game if word is valid
	public static void hangmanMain(){
		
		System.out.print("\033[H\033[2J");
		
		//setting up reader and word
		java.util.Scanner reader = new java.util.Scanner(System.in);
		String word = "";
		
		//setting up conditions to make sure the word is valid (4 unique letters)
		final int lengthConditionValue = 4;
		boolean lengthConditions = false;
		
		boolean uniqueCharacterConditions = false;
		boolean charactersConditions = false;
		boolean finalConditions = false;
		
		//checking conditions
		while (!finalConditions){
			
			//resetting conditions for new word
			lengthConditions = false;
			uniqueCharacterConditions = false;
			charactersConditions = false;
			
			System.out.println("Enter Word:");
			word = reader.next();
			
			//4 characters
			if(word.length() == lengthConditionValue){
				lengthConditions = true;
				
					//unique characters
				if(word.charAt(0) != word.charAt(1) && word.charAt(0) != word.charAt(2) && word.charAt(0) != word.charAt(3) && word.charAt(1) != word.charAt(2) && word.charAt(1) != word.charAt(3) && word.charAt(2) != word.charAt(3)){
					uniqueCharacterConditions = true;
				}else{
					System.out.println("Characters are not unique!");
				}
				
				//characters are letters (lowercase)
				if((word.charAt(0) >= 'a' && word.charAt(0) <= 'z') && (word.charAt(1) >= 'a' && word.charAt(1) <= 'z') && (word.charAt(2) >= 'a' && word.charAt(2) <= 'z') && (word.charAt(3) >= 'a' && word.charAt(3) <= 'z')){
					charactersConditions = true;
				}else{
					System.out.println("Characters are not between 'a' and 'z' (lowercase only)!");
				}
				
			}else{
				System.out.println("Word must be 4 characters long!");
			}
			
			
			//if all conditions are true, the loop will end
			if(lengthConditions && uniqueCharacterConditions && charactersConditions){
				finalConditions = true;
			}
		}
		
		//begins game
		System.out.print("\033[H\033[2J");
		System.out.println("Word met all conditions: beginning game!");
		WordGuesser.runGame(word);
	}
	
	//Starts runGame
	public static void wordleMain(){	
		//runGame(generateWord());
		WordleHard.runGame(WordleHard.generateWord());
	}
}