import java.util.Random;
import java.util.Scanner;
public class WordleHard{
	
	//Contains the list of possible words and returns one at random thanks to the random class
	public static String generateWord(){
		Random random = new Random();
		String[] words = {"ABOUT", "ALERT", "ARGUE", "BEACH", "ABOVE", "ALIKE", "ARISE","BEGAN", "ABUSE", "ALIVE", "ARRAY", "BEGIN", "ACTOR", "ALLOW", "ASIDE",	"BEGUN", "ACUTE", "ALONE", "ASSET",	"BEING"};
		
		return words[random.nextInt(20)];
	}
	
	//Takes a string "answer" and a character as a parameter. Returns true only if said character is present in the string
	public static boolean letterInWord(String answer,char letter){
		
		for(int i = 0;i<answer.length();i++){
			if (letter == answer.charAt(i)){
				return true;
			}
		}
		
		return false;
	}
	
	//takes a string "answer", a character and a position (in which that character is at in the guess string). Returns true only if the character is present in said position in the answer string it took.
	public static boolean letterInSlot(String answer, char letter, int position){
		return answer.charAt(position) == letter;
	}
	
	//uses the two previous methods to check if a given "guess" string contains characters that appear in the "answer" string, and if they're in the same position.
	//it returns an array representing how the "guess" string should be colored when printed, according to which of its characters are correct (ie present in the answer and/or in the same position).
	public static String[] guessWord(String answer, String guess, int[] letterCount){
		int[] letterCounts = new int[26];
		System.arraycopy(letterCount, 0, letterCounts, 0, 25);
		
		final String G = "green";
		final String Y = "yellow";
		final String W = "white";
		
		if (answer.equals(guess)){
			return new String[]{G, G, G, G, G};
		}
		
		String[] colors = {W, W, W, W, W};
		for(int j = 0;j<answer.length();j++){
			if (letterInSlot(answer, guess.charAt(j), j)){
				colors[j] = G;
				letterCounts[guess.charAt(j)-65] -= 1;				
			}
		}
		
		for(int l = 0;l<answer.length();l++){
			if ((letterInWord(answer, guess.charAt(l))) && (letterCounts[guess.charAt(l)-65]>0)){
				colors[l] = Y;
				letterCounts[guess.charAt(l)-65] -= 1;
			}
		}	
		return colors;
		
	}
	
	//prints the "guess" string with colors that represent if the letters in it are present in the "answer" string.
	//doesn't return anything and takes only the guess parameter and the array written by the guessword method.
	public static void presentResults(String guess, String[] colors){
		for(int i = 0;i<5;i++){
			if("green"==colors[i]){
				System.out.print("\u001B[32m"+ guess.charAt(i)+"\u001B[0m");
			}else if("yellow"==colors[i]){
				System.out.print("\u001B[33m"+ guess.charAt(i)+"\u001B[0m");
			}else{
				System.out.print(guess.charAt(i));
			}
		}
		System.out.println("");
	}
	
	//takes inputs of the user's guesses and returns it all in uppercase
	public static String readGuess(){
		Scanner reader = new Scanner(System.in);
		String guess = "";
		
		System.out.println("Enter a guess: ");
		guess = reader.next();
		
		while (!(guess.length() == 5)){
			System.out.println("Invalid guess");
			System.out.println("Enter a guess: ");
			guess = reader.next();		
		}
		
		return guess.toUpperCase();
	}
	
	public static int[] countLetters(String answer){
		int[] letterCount = new int[26];
		for(int i = 0;i<5;i++){
			letterCount[answer.charAt(i)-65] += 1;
		}
		return letterCount;
	}
	
	//uses previous methods to make a complete wordle game!
	public static void runGame(String answer){
		int attempts = 6;
		String guess = "";
		int[] letterCount = countLetters(answer);
		//while the user hasn't won by guessing the correct word or lost by failing too many attempts
		while ((attempts>0) && (!guess.equals(answer))) {
			guess = readGuess();
			presentResults(guess, guessWord(answer, guess, letterCount));
			attempts -= 1;
			if (attempts>0){
				System.out.println("Attempts remaining: " + attempts);}
		}
		
		if (guess.equals(answer)){
			System.out.println("You Win!");
		} else{
			System.out.println("You didn't find the word. Try again.");
		}
	}
}