public class WordGuesser{
	
	//asks user for word and calls teh runGame method
	public static void main(String []args){
		java.util.Scanner reader = new java.util.Scanner(System.in);
		System.out.println("Input a word to guess:");
		runGame(reader.next());
	}
	
	//sees if theres inputted letter in word and sends (first) location of it
	public static int isLetterInWord(String word, char c){	
		//place of word
		int place = 0;
		//goes through all chars of the word (1-4 or 0-3)
		while (place < 4){
			//verifies if inputted char is the same as letter of the given place
			if (word.charAt(place) == c){
				//returns given place and ends method
				return place;
			}
			//goes to the next place
			place++;
		}
		//if the inputted char was not in the word method will go here and return -1
		return -1;
		}
	
	public static void printWord(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3){
		String ans = "";
		//array to be able to look at all boolean inputs in a single while loop
		java.util.ArrayList<Boolean> list = new java.util.ArrayList<>();
		list.add(letter0);
		list.add(letter1);
		list.add(letter2);
		list.add(letter3);
		//just counts where we are at in the array
		int i = 0;
		//while we're in the array basically
		while (i < list.size()) {
			//if the inputted boolean at the given place is true, we add the letter of the word to that placement
			if (list.get(i)){
				ans = ans + word.charAt(i);
			//otherwise we add a _ or empty place
			} else{
				ans += "_";
			}
			//next position of the array
			i++;
		}
		//prints the current word with the letters we have guessed seen
		System.out.println(ans);
	}
	
	//calls other methods and loops them to run game
	public static void runGame(String word){
		boolean letter0 = false;
		boolean letter1 = false;
		boolean letter2 = false;
		boolean letter3 = false;
		
		int place = 0;
		final int guesses = 6;
		int misses = 0;
		
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		//while we don't have all the letters as true and we haven't run out of guesses
		while ( (!(letter0 && letter1 && letter2 && letter3)) && !(guesses == misses)){
			//asks user to input new letter
			System.out.println("Guess a letter:");
			//checks if the letter is in the word + the placement
			place = isLetterInWord(word, reader.next().charAt(0));
			//changes the boolean of the given placement to true if it was there
			if (place == 0){
				letter0 = true;
			}else if (place == 1){
				letter1 = true;
			}
			else if (place == 2){
				letter2 = true;
			}
			else if (place == 3){
				letter3 = true;
			//otherwise it adds a miss, else if just to be sure
			}else if (place == -1){
				misses++;
			}
			//prints the letter that have been guessed of the word in their correct placement
			printWord(word, letter0, letter1, letter2, letter3);
		}
		
		//if we ran out of guesses we lost, otherwise it means the while loop ended because all the letters were guessed, so we won!
		if (misses == guesses){
			System.out.println("You Lost.");
		} else{
			System.out.println("You Won!");
		}
	}
}