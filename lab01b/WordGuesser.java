public class WordGuesser{
	
	//asks for word and starts the game if word is valid
	public static void main(String[] args){
		
		System.out.print("\033[H\033[2J");
		
		//setting up reader and word
		java.util.Scanner reader = new java.util.Scanner(System.in);
		String word = "";
		
		//setting up conditions to make sure the word is valid (4 unique letters)
		final int lengthConditionValue = 4;
		boolean lengthConditions = false;
		
		boolean uniqueCharacterConditions = false;
		boolean charactersConditions = false;
		boolean finalConditions = false;
		
		//checking conditions
		while (!finalConditions){
			
			//resetting conditions for new word
			lengthConditions = false;
			uniqueCharacterConditions = false;
			charactersConditions = false;
			
			System.out.println("Enter Word:");
			word = reader.next();
			
			//4 characters
			if(word.length() == lengthConditionValue){
				lengthConditions = true;
				
					//unique characters
				if(word.charAt(0) != word.charAt(1) && word.charAt(0) != word.charAt(2) && word.charAt(0) != word.charAt(3) && word.charAt(1) != word.charAt(2) && word.charAt(1) != word.charAt(3) && word.charAt(2) != word.charAt(3)){
					uniqueCharacterConditions = true;
				}else{
					System.out.println("Characters are not unique!");
				}
				
				//characters are letters (lowercase)
				if((word.charAt(0) >= 'a' && word.charAt(0) <= 'z') && (word.charAt(1) >= 'a' && word.charAt(1) <= 'z') && (word.charAt(2) >= 'a' && word.charAt(2) <= 'z') && (word.charAt(3) >= 'a' && word.charAt(3) <= 'z')){
					charactersConditions = true;
				}else{
					System.out.println("Characters are not between 'a' and 'z' (lowercase only)!");
				}
				
			}else{
				System.out.println("Word must be 4 characters long!");
			}
			
			
			//if all conditions are true, the loop will end
			if(lengthConditions && uniqueCharacterConditions && charactersConditions){
				finalConditions = true;
			}
		}
		
		//begins game
		System.out.print("\033[H\033[2J");
		System.out.println("Word met all conditions: beginning game!");
		runGame(word);
	}
	
	//checks if letter is in the word
	public static int isLetterInWord(String word, char inputtedLetter){
		
		//setting up a value for invalid (could technically be anything besides 0-3)
		final int invalid = -1;
		
		//loop until count reaches word length or until it finds the letter
		for (int count = 0; count < word.length();count++){
			if (inputtedLetter == word.charAt(count)){
				return count;
			}
		}
		//if the loop ended and letter was not in, return invalid (-1 in this case)
		return invalid;
	}
	
	//shows word based on places revealed
	public static void printWord(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3){
		
		boolean[] letterBooleans = {letter0, letter1, letter2, letter3};
		
		//setting up the character defauls (will be displayed as ____ if nothing is guessed)
		char[] display = {'_', '_', '_', '_'};
		
		//changing the '_' characters to the letter if they are guessed
		for (int i = 0; i<letterBooleans.length; i++){
			if (letterBooleans[i]){
				display[i] = word.charAt(i);
			}
		}
		
		//displayed the whole word by adding the characters
		System.out.println("" + display[0] + display[1] + display[2] + display[3]);
	}
	public static void runGame(String word){
		
		//setting up booleans to see if letters were guessed
		boolean[] letterBooleans = {false,false,false,false};
		
		//setting up win/loss  condition variables
		boolean winStatus = false;
		int misses = 0;
		
		//amount of misses needed for a loss
		final int missLimit = 6;
		
		//loop for while player hasn't won and while the amount of misses is under 6
		while (!winStatus && misses < missLimit){
			
			//getting letter
			java.util.Scanner reader = new java.util.Scanner(System.in);
			System.out.println("Enter letter:");
			char c = reader.next().charAt(0);
		
			int place = isLetterInWord(word, c);
			
			//boolean used to check if any letter was changed in the following loop
			boolean ifMiss = false;
			
			//checking if letter matched with any of the characters
			for (int i = 0; i<4; i++){
			if (place == i && !letterBooleans[i]){
				System.out.print("\033[H\033[2J");
				letterBooleans[i] = true;
				System.out.println("'" + c + "' is in word!");
				System.out.println("Current misses: " + misses);
				ifMiss = true;
			}else if (i==3 && !ifMiss){
				System.out.print("\033[H\033[2J");
				misses++;
				System.out.println("'" + c + "' is NOT in word!");
				System.out.println("Current misses: " + misses);
			}
			}
			//if all letters are guessed, exits the loop and wins
			if (letterBooleans[0] && letterBooleans[1] && letterBooleans[2] && letterBooleans[3]){
				winStatus = true;
			}
			
			//prints the word with the letters found
			printWord(word, letterBooleans[0], letterBooleans[1], letterBooleans[2], letterBooleans[3]);
		}
		
		//if player exited the loop by winning, show win screen
		if (winStatus){
			System.out.println("You won! The word was '" + word + "'");
		//if player exited the loop by missing too much, show loss screen
		}else{
			System.out.println("You lost! The word was '" + word + "'");
		}
		
	}
}